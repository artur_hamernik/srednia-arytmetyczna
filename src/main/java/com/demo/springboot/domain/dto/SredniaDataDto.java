package com.demo.springboot.domain.dto;

//import com.demo.springboot.service.AverageService;
//import org.springframework.stereotype.Service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class SredniaDataDto {


    private String srednia;
    private Double wynik;
    private static List<Double> lista = new ArrayList<Double>();
    public SredniaDataDto(){}
    public SredniaDataDto(String srednia1) throws Exception {
        if(srednia1.isEmpty()){
            throw new Exception("Average can not be null");
        }
        else {
            this.srednia = srednia1;
            oblicz(srednia);
        }
    }
    private static void getNumbers(String s) {
        List<Double> l = new ArrayList<Double>();
        char[] temp = s.toCharArray();
        for(int i = 0; i < temp.length; i++){
            if(temp[i] == '-' && Character.isDigit(temp[i+1])){
                String po = "-";
                i++;
                while(i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i+1]))){
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
            else if(Character.isDigit(temp[i]) ){
                String po = "";
                while(i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i+1]))){
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
        }
        lista = l;
    }
    private void oblicz(String s){
        getNumbers(s);
        if(lista.isEmpty()){
            System.out.println("Lista jest pusta podaj wartosci");
        }
        else{
            Double temp = 0.0;
            for (int i = 0; i < lista.size(); i++){
                temp += lista.get(i);
            }
            wynik = temp/lista.size();
        }
    }
    public void setSrednia(String srednia) throws Exception {
        if(srednia == null){
            throw new Exception("Srednia musi miec wartosc");
        }
        else {
            this.srednia = srednia;
            oblicz(this.srednia);
        }
    }


    public Double getWynik() {
        return wynik;
    }

 /*   @Override
    public SredniaDataDto oblicz() {
        return null;
    }*/
}

