package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.SredniaDataDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
/*
@Controller
@RequestMapping("/api")
public class SredniaApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SredniaApiController.class);
    @Autowired
    private SredniaDataDto sredniaDataDto;

    @RequestMapping(value="/document/{srednia}", method = RequestMethod.GET)
    public ResponseEntity<SredniaDataDto> testDocument(@PathVariable("srednia") String srednia) throws Exception {

        sredniaDataDto.setSrednia(srednia);
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", sredniaDataDto.getWynik());

        return new ResponseEntity<>(sredniaDataDto, HttpStatus.OK);
    }
}*/
@Controller
@RequestMapping("/api")
public class SredniaApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SredniaApiController.class);

    @RequestMapping(value = "average/{srednia}", method = RequestMethod.GET)
    public ResponseEntity<SredniaDataDto> testDocument(@PathVariable("srednia") String srednia) throws Exception {

        SredniaDataDto oblicz = new SredniaDataDto(srednia);
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", oblicz.getWynik());

        return new ResponseEntity<>(oblicz, HttpStatus.OK);
    }

    @Autowired
    SredniaDataDto sredniaDataDto;
    @RequestMapping(value = "/averagewired/{srednia}", method = RequestMethod.GET)
    public ResponseEntity<SredniaDataDto> testDocument2(@PathVariable("srednia") String srednia) throws Exception {
        sredniaDataDto.setSrednia(srednia);
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", sredniaDataDto.getWynik());

        return new ResponseEntity<>(sredniaDataDto, HttpStatus.OK);
    }
    @Autowired
    SredniaDataDto sredniaDataDto2;
    @RequestMapping(value = "/averagewired", method = RequestMethod.GET)
    public ResponseEntity<SredniaDataDto> testDocument3(@RequestParam(value = "srednia", required = false) String srednia) throws Exception {
        sredniaDataDto2.setSrednia(srednia);
        LOGGER.info("######### testDocuemnt działa!!!!##########");
        LOGGER.info("######### Srednia wynosi {} ", sredniaDataDto2.getWynik());

        return new ResponseEntity<>(sredniaDataDto2, HttpStatus.OK);
    }


}
